var express = require('express');
var router = express.Router();
var path = require("path")
var fs = require("fs")

var md = require('markdown-it')({
  html:         true,        // Enable HTML tags in source
  xhtmlOut:     true,        // Use '/' to close single tags (<br />).
                              // This is only for full CommonMark compatibility.
  breaks:       true,        // Convert '\n' in paragraphs into <br>
  langPrefix:   'language-',  // CSS language prefix for fenced blocks. Can be
                              // useful for external highlighters.
  linkify:      true,        // Autoconvert URL-like text to links

  // Enable some language-neutral replacement + quotes beautification
  // For the full list of replacements, see https://github.com/markdown-it/markdown-it/blob/master/lib/rules_core/replacements.js
  typographer:  true,

  // Double + single quotes replacement pairs, when typographer enabled,
  // and smartquotes on. Could be either a String or an Array.
  //
  // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
  quotes: '“”‘’',

  // Highlighter function. Should return escaped HTML,
  // or '' if the source string is not changed and should be escaped externally.
  // If result starts with <pre... internal wrapper is skipped.
  highlight: function (/*str, lang*/) { return ''; }
});

function getMdPath(name) {

  return path.resolve(__dirname.replace(path.sep + "routes", ""), 'markdown') +  path.sep + name
} 

function getMdFile(name) {

  var fileMd = md.render(fs.readFileSync(getMdPath(name)).toString())
  console.log("GET MD_FILE - " + name + " - FILE : " + fileMd)
  return fileMd
   
}


function getProject() {

    var projectDiv = " ";
    var projectNumber = 1;

    fs.readdirSync(getMdPath("project")).forEach(file => {

      projectNumber += 1

      if(projectNumber == 2) {

        projectDiv += "<div class='w-100'></div>";
        projectNumber = 0;

      }

      var exampleForm =  '<div class="exBox col-lg container-md"><img id="exImg" class="projet-image img-fluid" src="./images/' + file.replace('.md', ".png") + '"><div class="subBox">' + getMdFile("project" + path.sep + file) +'</div></div>';
      
      projectDiv += exampleForm
    })

    console.log("PROJECT_AUTO_READER - " + projectDiv)
    return projectDiv


    

}


/* GET home page. */
router.get('/', function(req, res, next) {
  var myselfFile = getMdFile("myself.md").toString()
  res.render('index', { title: 'Raphix' , myself: myselfFile, projets: getProject()});
});

module.exports = router;
