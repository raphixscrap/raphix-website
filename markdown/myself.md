<img class="myselfImage" id="exImg" class="img-fluid" src="./images/affiche.png">

# **RaphixScrap**

<div style="text-align: justify;">

RaphixScrap ou Raphix pour les intimes ! Je développe depuis plusieurs années : en Web, sur Node JS, en Java ... **Bref, je m'amuse comme je peux !** Je fais aussi du Piano et (un tout petit peu) d'arrangement et de composition ! J'essaye de développer des trucs utiles <i class='fa fa-smile'></i> ! 

</div>

## Mes jeux du momment

<div class="my-games">
    <div class="game">
        <img class="game-picture" src="https://image.api.playstation.com/vulcan/img/cfn/11307x4B5WLoVoIUtdewG4uJ_YuDRTwBxQy0qP8ylgazLLc01PBxbsFG1pGOWmqhZsxnNkrU3GXbdXIowBAstzlrhtQ4LCI4.png">
        <p>Minecraft - Java Edition</p>
    </div>
    <div class="game">
        <img class="game-picture" src="https://media.indiedb.com/images/games/1/15/14730/IFSCL_450_codelyoko_chapter1b.png">
        <p>IFSCL</p>
    </div>
</div>