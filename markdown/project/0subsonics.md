# **Subsonics**

C'est un bot <i class='fa-brands fa-discord'></i>  **Discord** accompagné d'une interface Web qui permet de lire du contenu de <i class='fa-brands fa-youtube'></i> **Youtube** et <i class='fa-brands fa-soundcloud'></i> **SoundCloud** et de le diffuser dans un **salon vocal**.

__**Réalisé en 2021**__

**__Lien :__** <a style="color: white;" href="https://git.raphix.fr/lib/loguix">https://git.raphix.fr/lib/loguix</a>