# **Loguix**


C'est un petit <i class='fa-brands fa-node-js'></i> **Package  Node JS** permettant de générer des fichiers de log avec prise en charge des erreurs.

__**Réalisé en 2021**__

**__Lien :__** <a style="color: white;" href="https://git.raphix.fr/lib/loguix">https://git.raphix.fr/lib/loguix</a>